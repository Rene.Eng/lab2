package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    int maxSize = 20;
    List<FridgeItem> content;

    public Fridge(){
        content = new ArrayList<>();
    }

    public int totalSize(){
        return maxSize;
    }

    @Override
    public int nItemsInFridge() {
        return content.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (this.nItemsInFridge() < maxSize) {
            content.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (content.contains(item)){
            content.remove(item);
            return;
        }
        throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge() {
        content.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem item : content){
            if (item.hasExpired()){
                expiredFood.add(item);
            }
        }
        content.removeAll(expiredFood);
        return expiredFood;
    }
}
